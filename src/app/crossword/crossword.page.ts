import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import * as moment from 'moment';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HostListener } from '@angular/core';
import { JsonPipe } from '@angular/common';
import { promise } from 'protractor';
import { isFormattedError } from '@angular/compiler';
import { runInThisContext } from 'vm';

@Component({
  selector: 'crossword-play',
  templateUrl: './crossword.page.html',
  styleUrls: ['./crossword.page.scss'],
})
export class CrosswordPage implements OnInit {

  scheme: any;
  summary: any;
  activity: any;
  game: any;
  activityForm: FormGroup;
  activityForms: FormGroup[] = [];
  sid: number;
  pid: number;
  puzzleId: number;
  word: any;
  throughHint: boolean = false;
  input: boolean = false;
  resultDump: any;
  complexity: number;
  ConditionsChecked: boolean = false;
  reset: boolean = false
  challenge: any;
  cid: number;
  activeChallengeId: number;
  hintCondition: boolean = false;
  pointsForHints: number;
  circles: any;
  minimumBalance: boolean = true;
  gameDump: any;
  message: any;
  leastPoints: number;
  miniGameUse: number;
  completed: boolean;
  resetPropertyInstance: any;
  allowanceForHints: boolean;

  datetime: { history: string, now: string, tomorrow: string };

  constructor(
    private fb: FormBuilder,
  ) {
  }

  //listening to the message sent to iframe
  @HostListener('window:message', ['$event'])
  onMessage($event: MessageEvent) {
    let self = this;
    const data = $event.data;
    this.consoleLog("The event data ", data);
    if (data !== undefined && data != null) {
      const message = JSON.parse(data);
      //const message = data;
      this.consoleLog("message in the iframe=====", message);
      this.scheme = message.scheme;
      this.activity = message.activity;
      this.puzzleId = message.gameStateId;
      this.summary = message.summary;
      this.pid = message.pid;
      this.game = message.game;
      this.miniGameUse = message.miniGameUse;
      this.gameDump = message.gameDump;
      this.leastPoints = message.leastPoint;
      this.cid = message.challengeId;
      this.activeChallengeId = message.activeChallengeId;
      this.allowanceForHints = message.allowanceForHints;
    }

    this.consoleLog("scheme in iframe--->",JSON.stringify(this.scheme));
    this.consoleLog("summary in iframe--->",JSON.stringify(this.summary));
    this.consoleLog("activity in iframe--->",JSON.stringify(this.activity));
    this.consoleLog("pid in iframe--->",this.pid);
    this.consoleLog("game in iframe--->",JSON.stringify(this.game));
    this.consoleLog("MiniGameUse in iframe--->",this.miniGameUse);
    this.consoleLog("gameDump in iframe--->",this.gameDump);
    this.consoleLog("cid in iframe--->",this.cid);
    this.consoleLog("activeChallengeId in iframe--->",this.activeChallengeId);
    this.consoleLog("puzzleId/gameStateId in iframe--->",this.puzzleId);
    this.consoleLog("allowanceForHints in iframe--->",this.allowanceForHints);

    if (this.summary !== null && this.summary !== undefined) {
      const pp = JSON.parse(JSON.stringify(this.scheme.propertyPermissions));
      let propertyPermissions = pp.filter((p: any) => p.dataProvider.id === 1);
      if (this.summary.conditions !== undefined && this.summary.conditions != null) {
        const conditions = this.summary.conditions.filter((c: any) => c.property !== undefined && c.property !== null);
        if (conditions !== null && conditions !== undefined) {
          conditions.forEach(
            (c: any) => {
              const p = propertyPermissions.find((p: any) => p.property.id === c.property);
              if (p.property.translationKey === "COMPLEXITY") {
                this.complexity = c.value;
              } else if (p.property.translationKey === "MINIGAMESTATE_ID") {
                this.puzzleId = c.value;
              } else if (p.property.translationKey === "THROUGH_HINT") {
                this.hintCondition = true;
                const points = this.summary.points.filter((p: any) => p.point !== undefined && p.point !== null);
                if(points !== null && points !== undefined){
                  points.forEach(
                    (p:any) => {
                      this.pointsForHints = p.value;
                    }
                  )
                }
              }
            });
        }
      }
    }

    if (this.scheme !== null && this.scheme !== undefined) {
      this.sid = this.scheme.id;
    }

    this.datetime = {
      history: moment().subtract(2, 'weeks').format('YYYY-MM-DD'),
      now: moment().toISOString(),
      tomorrow: moment().add(1, 'days').format('YYYY-MM-DD'),
    };

    this.activityForm = this.fb.group({
      gameDescriptor: [this.sid, Validators.compose([Validators.required])],
      dataProvider: [1, Validators.compose([Validators.required])],
      miniGameUse: this.miniGameUse,
      date: [this.datetime.now],
      propertyInstances: this.fb.array([]),
      property: null,
      value: null,
      players: [[this.pid], Validators.compose([Validators.required])],
    });

    if (self.activity !== undefined) {
      self.getGameDump(self.activity).then(function (game) {
        self.onGameReady(game);
      });
    }
     else {
      this.consoleLog("GameDump before onGameReady: ",this.gameDump);
      this.getGameForGameDump(this.gameDump).then(function (game) {
        self.consoleLog("Game before onGameReady: ",game);
        self.onGameReady(game);
      });
    }

  }


  async ngOnInit() {
    
  }

  getGameDump(activity: any): Promise<any> {
    var self = this;
    return new Promise(function (resolve, reject) {
      if(typeof activity === 'string'){
        activity = JSON.parse(activity);
      }

      if (activity.propertyInstances !== undefined && activity.propertyInstances !== null) {
        for (const instance of self.activity.propertyInstances) {
          if (instance.property !== undefined && instance.property !== null) {
            if (instance.property.translationKey === 'MINIGAME_DATA') {
              self.resultDump = instance.value;
              if (self.resultDump !== null) {
                let gameDump = JSON.parse(self.resultDump);

                let Crossword = self.getCrosswordObject();
      
                let game = new Crossword(gameDump, document.querySelector(".gameArea"), true, self.leastPoints, self.allowanceForHints);
                resolve(game);
              }
            }
          }
        }
      }
  })
  }

  async onGameReady(game){
    var self = this;
    this.game = game;
    game.on('word', function (e) { self.wordGuessed(e); });
    game.on('hint', function (e) { self.hintTaken(e); });
    game.on('complete', function (e) { self.puzzleCompleted(e); });
    //game.on('input', function (e) { self.inputEntered(e);})
    game.resize();
  }

  getGameForGameDump(gameDump: any): Promise<any> {
    var self = this;
    return new Promise(function (resolve, reject) {
          let Crossword = self.getCrosswordObject();
          self.consoleLog("GameDump before Crossword hereee", JSON.stringify(gameDump));
      
          let game = new Crossword(gameDump, document.querySelector(".gameArea"), false, self.leastPoints, self.allowanceForHints);
          self.consoleLog("HERE activeChallengeId", self.activeChallengeId);
          if (self.activeChallengeId === undefined || self.activeChallengeId === null) {
            document.querySelector('.gameArea > .question > .buttons').remove();
          }
          resolve(game);
    })
  }

  createPropertyForm(): FormGroup {
    return this.fb.group({
      property: [null],
      value: [null],
    });
  }
  addPropertyForm() {
    const formArray = this.activityForm.get('propertyInstances') as FormArray;
    formArray.push(this.createPropertyForm());
  }

  addPropertiesAndActivity() {
    let dump = this.game.getGameState();
    let word = this.word;
    let hint = this.throughHint;
    let completed = this.completed;
    let resetPuzzle = this.reset;
    this.gameDump = this.game.getGameState();
    
    const pp = JSON.parse(JSON.stringify(this.scheme.propertyPermissions));

    const propertyPermissions = pp
      .filter((p: any) => p.dataProvider.id === 1);
    
    propertyPermissions.forEach(
      (p: any) => {
        switch (p.property.translationKey) {
          case "MINIGAME_DATA":
            {
              p.value = JSON.stringify(dump);
              p.property = p.property.id;
            }
            break;
          case "MINIGAMESTATE_ID": {
            p.value = this.puzzleId;
            p.property = p.property.id;
          }
            break;
          case "COMPLEXITY": {
            p.value = this.complexity;
            p.property = p.property.id;
          }
            break;
          case "WORD_ENTERED": {
            p.value = JSON.stringify(word);
            p.property = p.property.id;
          }
            break;
          case "THROUGH_HINT": {
            p.value = hint;
            p.property = p.property.id;
          }
            break;
          case "WORD_CORRECT": {
            if (word !== undefined && word !== null) {
              p.value = true;
            } else {
              p.value = false;
            }
            p.property = p.property.id;
          }
            break;
          case "PUZZLE_COMPLETE":{
            if(completed == true){
              p.value = true;
            } else {
              p.value = false;
            }
            p.property = p.property.id;
          }
            break;
            case "RESET":{
              if(this.reset == true){
                p.value = true;
              } else {
                p.value = false;
              }
              p.property = p.property.id;
            }
              break;
          default: {
            p.property = p.property.id;
            p.value = null;
          }
            break;

        }

      }
    );

    this.consoleLog("Show propertyPermissions to add RESET:", JSON.stringify(propertyPermissions));

    if(this.activityForm.value.propertyInstances === null || !(this.activityForm.value.propertyInstances.length > 0)){
      propertyPermissions.forEach(() => { this.addPropertyForm(); });
    }
    this.activityForm.get('propertyInstances').patchValue(propertyPermissions);

    //add the activity
    this.activityForm.removeControl('property');
    this.activityForm.removeControl('value');
    const form = this.activityForm.value;
    form.date = moment(form.date).valueOf();
    this.consoleLog("Activity:",form);
    this.activityForms.push(form);
  }
  
  wordGuessed(e: any): void {
    this.word = e;
    this.addPropertiesAndActivity();
    this.word = null;
    this.saveGame();
  }

  hintTaken(e: any): void {
    this.throughHint = true;
    this.addPropertiesAndActivity();
    this.throughHint = false;
    this.saveGame();
  }

  inputEntered(e: any): void {
    this.input = true;
    this.addPropertiesAndActivity();
    this.saveGame();
  }

  puzzleCompleted(e: any): void {
    this.completed = true;
    this.addPropertiesAndActivity();
    this.completed = false;
    this.saveGame();
  }

  getCrosswordObject(): any {
    //var AudioContext = window.AudioContext;// || window.webkitAudioContext;

    var EVENTS = {
      START: "start",
      SELECT: "select",
      FOCUS: "focus",
      INPUT: "input",
      WORD: "word",
      HINT: "hint",
      HINT_WORD: "wordhint",
      COMPLETE: "complete"
    }

    var ARROW_UP = 38,
      ARROW_DOWN = 40,
      ARROW_LEFT = 37,
      ARROW_RIGHT = 39,
      SOUND_OK = "sounds/ok.mp3",
      SOUND_WIN = "sounds/win.mp3",
      R = {};
    var PI2 = Math.PI * 2;

    var PIXEL_RATIO = (function () {
      var ctx = document.createElement("canvas").getContext("2d"),
        dpr = window.devicePixelRatio || 1
      //   bsr = ctx.webkitBackingStorePixelRatio ||
      //       ctx.mozBackingStorePixelRatio ||
      //       ctx.msBackingStorePixelRatio ||
      //       ctx.oBackingStorePixelRatio ||
      //       ctx.backingStorePixelRatio || 1;

      // return dpr / bsr;
      return dpr;
    })();

    function Crossword(state, parent, result, leastPoints, allowanceForHints) {
      
      this.init(state, parent, result, leastPoints, allowanceForHints);
    }

    Crossword.prototype = {};

    Crossword.prototype.init = function (state, parent, result, leastPoints, allowanceForHints) {
  
      if (result) {
        this.mutable = false;
      }

      if(typeof state === 'string'){
        this.state = JSON.parse(state);
      } else{
        this.state = state;
      }
      
      this.parent = parent;
      this.leastPoints = leastPoints;
      this.allowanceForHints = allowanceForHints;

      var resultContent = parent.querySelector('.gameArea > .result');

      if (this.mutable === false) {
        parent.querySelector('.gameArea > .question').remove();
      } else if(resultContent) {
        parent.querySelector('.gameArea > .result').remove();
      }

      this.inputField = parent.querySelector('.gameArea > input');
      this.questionField = parent.querySelector(".qt");
      this.canvas = parent.querySelector(".crossword");
      this.ctx = this.canvas.getContext("2d");
      this.ctx.fillStyle = "white";
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

      this.previous = parent.querySelector(".previous");
      this.next = parent.querySelector(".next");
      this.hintBtn = parent.querySelector(".hint");

      this.grid = this.processGameState(this.state);
      this.listeners = {};

      this.initListeners();
      this.resize();
    }

    Crossword.prototype.on = function (eventType, callback) {
      if (!this.listeners[eventType]) this.listeners[eventType] = [];

      if (this.listeners[eventType].indexOf(callback) === -1)
        this.listeners[eventType].push(callback);
    }

    Crossword.prototype.off = function (eventType, callback) {
      if (this.listeners[eventType]) {
        var index = this.listeners[eventType].indexOf(callback);
        if (index !== -1) this.listeners[eventType].splice(index, 1);
      }
    }

    Crossword.prototype.fireEvent = function (eventType, e) {
      var listeners = this.listeners[eventType];
      if (listeners) {
        for (var i = 0; i < listeners.length; i++) {
          listeners[i].call(null, e);
        }
      }
    }

    Crossword.prototype.hint = function() {
      //let self = this;
      //self.leastPoints = self.state.balance;
      // this.state.checkForBalance().then(function(balance){
      //   if(balance) self.processHint();
      //   else alert("U heeft onvoldoende punten om een hint aan te vragen");
      // });

      if (this.leastPoints > 0 && this.allowanceForHints == true){
        this.processHint();
      } else{
        alert("U heeft onvoldoende punten om een hint aan te vragen, punten:" + this.leastPoints);
      }
      
    }

    Crossword.prototype.processHint = function () {

      var word = this.selected;
      var options = [];
      for (var i = 0; i < word.word.length; i++) {
        var block = this.getFocusBlock(i);
        if (this.grid[block.x][block.y].indexOf('*') !== 0) {
          options.push(i);
        }
      }

      if (options.length > 0) {
        var n = Math.floor(Math.random() * options.length);
        n = options[n];
        var block = this.getFocusBlock(n);
        this.acceptCharacter(block, word.word[n].toUpperCase());
        if (options.length == 1) this.acceptAnswer(word);
        this.state.balance -= 1;
        this.fireEvent(EVENTS.HINT, {
          type: EVENTS.HINT,
          cost: 1,
          balance: this.state.balance,
          word: word,
          hint: n,
          timestamp: (new Date()).getTime()
        });
        return true;
      }
    }

    Crossword.prototype.hintWord = function() {
      //let self = this;
      if (this.leastPoints > 0  && this.allowanceForHints == true){
        this.processHint();
      } else{
        alert("U heeft onvoldoende punten om een hint aan te vragen, punten:" + this.leastPoints);
      }
      // this.state.checkForBalance().then(function(balance){
      //   if(balance) self.processHintWord();
      //   else alert("U heeft onvoldoende punten om een woord hint aan te vragen");
      // });
    }

    Crossword.prototype.processHintWord = function () {
      var word = this.selected;
      if (!word) {
      } else {
        this.state.balanece -= 5;
        this.fireEvent(EVENTS.HINT_WORD, {
          type: EVENTS.HINT_WORD,
          cost: 5,
          balance: this.state.balance,
          word: word,
          timestamp: (new Date()).getTime()
        });
        this.acceptAnswer(word);
      }
    }

    Crossword.prototype.resize = function () {
      this.autoSize();
      this.autoCanvasSize();
      this.redraw();
    }

    Crossword.prototype.autoSize = function () {
      var alignElement = this.parent;
      var width = alignElement.offsetWidth * PIXEL_RATIO - 50;
      var height = alignElement.offsetHeight * PIXEL_RATIO - 50;
      var sizeH = width / this.grid.length;
      var sizeV = height / this.grid[0].length;

      this.size = Math.floor(Math.min(sizeH, sizeV));
    }

    Crossword.prototype.autoCanvasSize = function () {
      this.width = this.grid.length * this.size;
      this.height = this.grid[0].length * this.size;
      this.setCanvasSize(this.width, this.height);
    }

    Crossword.prototype.setCanvasSize = function (width, height) {
      this.canvas.width = width;
      this.canvas.height = height;
      this.canvas.style.width = width / PIXEL_RATIO + "px";
      this.canvas.style.height = height / PIXEL_RATIO + "px";
    }

    Crossword.prototype.processGameState = function (state) {
      if(typeof state === 'string'){
        state = JSON.parse(state);
      } 

      var words = this.words = state.words;
      if (!state.duration) {
        this.fireEvent(EVENTS.START, {
          type: EVENTS.START,
          cost: 10,
          balance: this.state.balance,
          gameId: state.id,
          timestamp: (new Date()).getTime()
        });
      }
      if (!state.balance) state.balance = 0;
      this.started = new Date();
      for (var i = 0; i < words.length; i++) {
        var word = words[i];
        if (!word.input) {
          word.input = "---";
          while (word.input.length < word.word.length) word.input += "-";
        } else if (/^\*+$/.test(word.input)) word.complete = true;
      }
      if (state.selected && this.mutable !== false) {
        this.selected = words[state.selected];
        this.focus = state.focus;
        this.questionField.innerText = this.selected.question;
      }
      return this.generateGrid(words);
    }

    Crossword.prototype.getGameState = function () {
      if (!this.state.finished) {
        var duration = this.state.duration || 0;
        var end = new Date();
        duration += end.getTime() - this.started.getTime();
        this.state.duration = duration;
        this.started = end;

        if (this.selected) {
          this.state.selected = this.words.indexOf(this.selected);
          this.state.focus = this.focus;
        } else this.state.selected = false;
      }
      return this.state;
    }

    Crossword.prototype.generateGrid = function (words) {
      //Determine Limits
      var limits = { x: 0, y: 0 }; //Skip if limits is set
      for (var i = 0; i < words.length; i++) {
        var word = words[i];
        if (word.x > limits.x) limits.x = word.x;
        if (word.y > limits.y) limits.y = word.y;
        if (word[word.d] + word.word.length > limits[word.d]) limits[word.d] = word[word.d] + word.word.length;
      }

      this.offsetX = 0;//(800 - (limits.x * this.size)) / 2;
      this.offsetY = 0;//(600 - (limits.y * this.size)) / 2;

      //Create Empty grid
      var grid = [];
      for (var x = 0; x < limits.x; x++) {
        grid[x] = [];
        for (var y = 0; y < limits.y; y++) {
          grid[x][y] = 0;
        }
      }

      //Fill Grid
      for (var i = 0; i < words.length; i++) {
        var word = words[i];
        var pos = { x: word.x, y: word.y };
        for (var n = 0; n < word.word.length; n++) {
          var char = "";
          if (word.input[n] === '*') char = '*' + word.word[n];
          else if (word.input[n] !== '-') char = word.input[n];
          var current = grid[pos.x][pos.y];
          if (current && current.indexOf('*') === 0 || (char === "" && current !== 0)) {
            char = current;
          }
          grid[pos.x][pos.y] = char.toUpperCase();
          pos[word.d]++;
        }
      }

      return grid;
    }

    Crossword.prototype.initListeners = function () {
      var self = this;
      var inputField = this.inputField;
      var keydownSupport = false;

      if (this.mutable !== false) {
        // this.previous.addEventListener("click", function (e) {
        //   self.selectPreviousWord();
        // });
        // this.next.addEventListener("click", function (e) {
        //   self.selectNextWord();
        // });
        this.hintBtn.addEventListener("click", function (e) {
          self.hint();
        });
      }
      this.canvas.addEventListener("mousemove", function (e) {
        self.mouseMoved(e)
      });
      this.canvas.addEventListener("click", function (e) {
        self.onClick(e);
        inputField.focus();
      });
      inputField.addEventListener("input", function (e) {
        if (!keydownSupport) self.inputEvent(e);
      });
      inputField.addEventListener("blur", function (e) {
        setTimeout(function () {
          inputField.focus();
        }, 10);
      });
      this.parent.addEventListener("keydown", function (e) {
        //document.body.addEventListener("keydown", function(e){
        var supported = self.keyDown(e);
        e.stopPropagation();
        keydownSupport = supported;
        if (supported) inputField.value = "";
      });
      inputField.focus();
    }

    Crossword.prototype.inputEvent = function (e) {
      var inputField = this.inputField;
      /*var debug = this.debug;
      debug.innerText = "InputEvent: inputType = " + e.inputType + ", data = " + e.data;*/
      if (e.inputType === "deleteContentBackward") this.deleteInput();
      else if (e.inputType === "insertText") {
        this.textInput(e.data);
      } else if (e.inputType === "insertCompositionText") {
        this.textInput(e.data[e.data.length - 1]);
      }

      e.preventDefault();
      e.stopPropagation();

      inputField.value = "---------------------";
    }

    Crossword.prototype.mouseMoved = function (e) {
    }

    Crossword.prototype.onClick = function (e) {
      var size = this.size;
      var grid = this.grid;
      var x = e.offsetX * PIXEL_RATIO;
      var y = e.offsetY * PIXEL_RATIO;
      x = Math.floor(x / size);
      y = Math.floor(y / size);

      if (x < grid.length && y < grid[x].length) {
        if (grid[x][y] === 0) {
          this.focus = false;
          this.deselect();
        } else {
          this.selectFrom({ x: x, y: y });
        }
      }
    }

    Crossword.prototype.userInput = function (block, input) {
      var vertical = this.getVerticalWord(block);
      if (vertical) {
        var focus = block.y - vertical.y;
        var str = vertical.input;
        vertical.input = str.substr(0, focus) + input + str.substr(focus + 1);
      }

      var horizontal = this.getHorizontalWord(block);
      if (horizontal) {
        var focus = block.x - horizontal.x;
        var str = horizontal.input;
        horizontal.input = str.substr(0, focus) + input + str.substr(focus + 1);
      }
    }

    Crossword.prototype.textInput = function (input) {
      if (/^[a-zA-Z]$/.test(input)) { //input character
        input = input.toUpperCase();
        this.charInput(input);
      }
    }

    Crossword.prototype.keyDown = function (e) {
      if (e.keyCode === 229) return false;
      if (e.keyCode == 8) this.deleteInput();
      else if (e.keyCode >= 37 && e.keyCode <= 40) this.arrowInput(e.keyCode);
      else if (e.keyCode >= 65 && e.keyCode <= 90) {
        var character = String.fromCharCode(e.keyCode).toUpperCase();
        this.charInput(character);
      } else return false;
      return true;
    }

    Crossword.prototype.deleteInput = function () {
      if (!this.selected) return;
      if (this.focus == 0) return;

      var word = this.selected;
      var pos = { x: word.x, y: word.y };
      pos[word.d] += this.focus;
      if (this.grid[pos.x][pos.y] === "") {
        this.previousFocus();
        var word = this.selected;
        var pos = { x: word.x, y: word.y };
        pos[word.d] += this.focus;
      }
      var old = this.grid[pos.x][pos.y];
      this.grid[pos.x][pos.y] = "";
      this.updateBlock(pos);
      var wasCorrect = (word.word[this.focus] == old);
      this.userInput(pos, "-");
      this.fireEvent(EVENTS.INPUT, {
        type: EVENTS.INPUT,
        word: word,
        focus: this.foucs,
        input: "-",
        correct: false,
        wasCorrect: wasCorrect,
        timestamp: (new Date()).getTime()
      });
    }

    //Maybe pressing down on a horizontal word should select the vertical as well
    Crossword.prototype.arrowInput = function (arrow) {
      if (this.selected) {
        if (arrow == ARROW_UP && this.selected.d == 'y') {
          this.previousFocus();
        } else if (arrow == ARROW_DOWN && this.selected.d == 'y') {
          this.nextFocus();
        } else if (arrow == ARROW_LEFT && this.selected.d == 'x') {
          this.previousFocus();
        } else if (arrow == ARROW_RIGHT && this.selected.d == 'x') {
          this.nextFocus();
        }
      }
    }

    Crossword.prototype.charInput = function (character) {
      if (!this.selected) return;
      var word = this.selected;
      if (this.focus >= word.word.length) return;
      var pos = { x: word.x, y: word.y };
      var wasCorrect = (word.word[this.focus] == old);
      pos[word.d] += this.focus;
      var old = this.grid[pos.x][pos.y];
      this.grid[pos.x][pos.y] = character;
      if (!this.nextFocus()) //nextFocus calls swapFocus which updates old and new focus fields, automatically drawing the input character;
        this.updateBlock(pos);
      this.userInput(pos, character);
      var correct = this.checkInput(pos);
      this.fireEvent(EVENTS.INPUT, {
        type: EVENTS.INPUT,
        word: word,
        focus: this.foucs,
        input: character,
        correct: correct,
        wasCorrect: wasCorrect,
        timestamp: (new Date()).getTime()
      });
    }

    Crossword.prototype.checkInput = function (pos) {
      var horizontal = this.getHorizontalWord(pos);
      var vertical = this.getVerticalWord(pos);

      var chime = false;
      if (horizontal && this.checkWord(horizontal)) {
        this.acceptAnswer(horizontal);
        chime = true;
        this.fireWordCompletedEvent(horizontal);
      }
      if (vertical && this.checkWord(vertical)) {
        this.acceptAnswer(vertical);
        chime = true;
        this.fireWordCompletedEvent(vertical);
      }

      if (this.selected.complete) {
        !this.selectNextWord(this.selected);
      }
      if (chime) this.playSound(SOUND_OK);
      return true;
    }

    Crossword.prototype.fireWordCompletedEvent = function (word) {
      this.fireEvent(EVENTS.WORD, {
        type: EVENTS.WORD,
        word: word,
        timestamp: (new Date()).getTime()
      });
    }

    Crossword.prototype.checkWord = function (word) {
      var valid = true;
      var answer = word.word.toUpperCase();
      var p = { x: word.x, y: word.y };
      for (var i = 0; i < word.word.length && valid; i++) {
        var input = this.grid[p.x][p.y];
        if (input != answer[i] && input.indexOf('*') !== 0) valid = false;
        p[word.d]++;
      } return valid;
    }

    Crossword.prototype.acceptAnswer = function (word) {
      var p = { x: word.x, y: word.y };
      for (var i = 0; i < word.word.length; i++) {
        var character = word.word[i];
        this.acceptCharacter(p, character.toUpperCase());
        p[word.d]++;
      }
      word.complete = true;
    }

    Crossword.prototype.getCurrentInput = function (word) {
    }

    Crossword.prototype.selectNextWord = function (word) {
      var candidate;
      if (word === undefined) word = this.selected;
      var index = this.words.indexOf(word);
      if (index === -1) index = 0;
      for (var i = 1; i < this.words.length; i++) {
        var n = (i + index) % this.words.length;
        candidate = this.words[n];
        if (!candidate.complete) break;
      }
      if (candidate.complete) {
        this.deselect();
        this.completeGame();
        return false;
      } else {
        this.select(candidate);

        return true;
      }
    }

    Crossword.prototype.selectPreviousWord = function (word) {
      var candidate;
      if (word === undefined) word = this.selected;
      var index = this.words.indexOf(word);
      if (index === -1) index = 0;
      for (var i = this.words.length - 1; i > 0; i--) {
        var n = (i + index) % this.words.length;
        candidate = this.words[n];
        if (!candidate.complete) break;
      }
      if (candidate.complete) {
        this.deselect();
        this.completeGame();
        return false;
      } else {
        this.select(candidate);
        return true;
      }
    }

    Crossword.prototype.completeGame = function () {
      var state = this.getGameState();
      if (!state.finished) {
        this.playSound(SOUND_WIN);
        this.questionField.innerText = "U heeft gewonnen";
        state.finished = this.started;
        state.started = undefined;
        this.fireEvent(EVENTS.COMPLETE, {
          type: EVENTS.COMPLETE,
          state: state,
          timestamp: (new Date()).getTime()
        });
      }
    }

    Crossword.prototype.deselect = function (word) {
      word = word || this.selected;
      if (!word) return;
      this.selected = false;
      this.questionField.innerText = "Klik op een leeg veld om een woord te selecteren.";
      this.redrawWord(word);
    }

    Crossword.prototype.select = function (word, preferredFocus) {
      if (this.mutable === false) return;
      if (word != this.selected) {
        var old = this.selected;
        this.selected = word;
        this.focus = false;
        if (preferredFocus === undefined) preferredFocus = 0;
        for (var i = 0; i < word.word.length; i++) {
          var f = (preferredFocus + i) % word.word.length;
          if (this.canFocus(f)) {
            this.focus = f;
            break;
          }
        }
        if (old) this.redrawWord(old);
        this.redrawWord(this.selected);
        this.fireEvent(EVENTS.SELECT, {
          type: EVENTS.SELECT,
          word: word,
          old: old,
          focus: this.focus,
          timestamp: (new Date()).getTime()
        });
        this.questionField.innerText = word.question;
      }
    }

    Crossword.prototype.getFocusBlock = function (focus) {
      if (focus === undefined) focus = this.focus;
      if (!this.selected) return;
      var block = { x: this.selected.x, y: this.selected.y };
      block[this.selected.d] += focus;
      return block;
    }

    Crossword.prototype.canFocus = function (focus) {
      var block = this.getFocusBlock(focus);
      var c = this.grid[block.x][block.y];
      if (c.indexOf('*') === 0 || c === '0') return false;
      else return true;
    }

    Crossword.prototype.nextFocus = function () {
      if (!this.selected) return;
      var newFocus = this.focus;
      while (newFocus + 1 < this.selected.word.length) {
        newFocus++;
        if (this.canFocus(newFocus)) {
          this.setFocus(this.selected, newFocus);
          return true;
        }
      } return false;
    }

    Crossword.prototype.previousFocus = function () {
      if (!this.selected) return;
      var newFocus = this.focus;
      while (newFocus > 0) {
        newFocus--;
        if (this.canFocus(newFocus)) {
          this.setFocus(this.selected, newFocus);
          return true;
        }
      } return false;
    }

    Crossword.prototype.swapFocus = function (word, oldFocus, newFocus) {
      var oldBlock = { x: word.x, y: word.y };
      var newBlock = { x: word.x, y: word.y };
      oldBlock[word.d] += oldFocus;
      newBlock[word.d] += newFocus;
      this.focus = newFocus;
      this.updateBlock(oldBlock);
      this.updateBlock(newBlock);
    }

    Crossword.prototype.setFocus = function (word, focus) {
      var old = this.focus;
      if (this.focus === focus) return;
      else if (this.canFocus(focus)) {
        this.swapFocus(word, this.focus, focus);
      }

      this.fireEvent(EVENTS.FOCUS, {
        type: EVENTS.FOCUS,
        word: this.selected,
        old: old,
        focus: this.focus,
        timestamp: (new Date()).getTime()
      });
    }

    Crossword.prototype.selectFrom = function (block) {
      var horizontal = this.getHorizontalWord(block);
      var horizontalFocus = horizontal ? block.x - horizontal.x : -1;
      var inSelection = this.isInSelection(block);
      if (horizontal && !inSelection) { //Select horizontal if available and nothing is selected
        this.select(horizontal, horizontalFocus);
        return;
      }

      var vertical = this.getVerticalWord(block);
      var verticalFocus = vertical ? block.y - vertical.y : -1;
      if (vertical && !inSelection) { //Select vertical if available and nothing is selected
        this.select(vertical, verticalFocus);
        return;
      }

      if (vertical && this.selected == horizontal && this.focus == horizontalFocus) { //Swap to vertical
        this.select(vertical, verticalFocus);
      } else if (horizontal && this.selected == vertical && this.focus == verticalFocus) { //swap to horizontal
        this.select(horizontal, horizontalFocus);
      } else if (this.selected == horizontal && this.focus != horizontalFocus) { //update focus
        this.setFocus(this.selected, horizontalFocus);
      } else if (this.selected == vertical && this.focus != verticalFocus) { //update focus
        this.setFocus(this.selected, verticalFocus);
      }
    }

    Crossword.prototype.isInSelection = function (block) {
      if (this.selected && this.selected.d == 'x') {
        if (block.y == this.selected.y &&
          block.x >= this.selected.x &&
          block.x < this.selected.x + this.selected.word.length) {
          return true;
        }
      } else if (this.selected && this.selected.d == 'y') {
        if (block.x == this.selected.x &&
          block.y >= this.selected.y &&
          block.y < this.selected.y + this.selected.word.length) {
          return true;
        }
      }
      return false;
    }

    Crossword.prototype.getVerticalWord = function (pos, includeCompleted) {
      for (var i = 0; i < this.words.length; i++) {
        var candidate = this.words[i];
        if (!includeCompleted && candidate.complete) continue;
        if (candidate.d == 'y' && pos.x == candidate.x && pos.y >= candidate.y && pos.y < candidate.y + candidate.word.length)
          return candidate;
      }
    }

    Crossword.prototype.getHorizontalWord = function (pos, includeCompleted) {
      for (var i = 0; i < this.words.length; i++) {
        var candidate = this.words[i];
        if (!includeCompleted && candidate.complete) continue;
        if (candidate.d == 'x' && pos.y == candidate.y && pos.x >= candidate.x && pos.x < candidate.x + candidate.word.length)
          return candidate;
      }
    }

    Crossword.prototype.blockToPos = function (block) {
      //Pixel ratio?
      var pos = { x: block.x, y: block.y };
      pos.x *= this.size;
      pos.y *= this.size;
      pos.x += this.offsetX + 0.5;
      pos.y += this.offsetY + 0.5;
      return pos;
    }

    Crossword.prototype.posToBlock = function (pos) {
      var block = { x: pos.x, y: pos.y };
      block.x -= this.offsetX;
      block.y -= this.offsetY;
      block.x /= this.size;
      block.y /= this.size;
      return block;
    }

    Crossword.prototype.redraw = function () {
      var ctx = this.ctx;
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      for (var x = 0; x < this.grid.length; x++) {
        for (var y = 0; y < this.grid[0].length; y++) {
          this.renderBlock(ctx, { x: x, y: y });
        }
      }
    }

    Crossword.prototype.redrawWord = function (word) {
      var block = { x: word.x, y: word.y };
      var dir = word.d;
      for (var i = 0; i < word.word.length; i++) {
        this.updateBlock(block);
        block[dir]++;
      }
    }

    Crossword.prototype.updateBlock = function (ctx, block) {
      if (ctx.x !== undefined && !block) {
        block = ctx;
        ctx = this.ctx;
      }

      var pos = this.blockToPos(block);
      var size = this.size;
      if (pos.x + size > 0 && pos.x < this.canvas.width &&
        pos.y + size > 0 && pos.y < this.canvas.height) {
        this.clearBlock(ctx, block);
        this.renderBlock(ctx, block);
      }
    }

    Crossword.prototype.clearBlock = function (ctx, block) {
      var pos = this.blockToPos(block);
      var size = this.size;
      ctx.clearRect(block.x * size, block.y * size, size, size);
    }

    Crossword.prototype.renderBlock = function (ctx, block) {
      var pos = this.blockToPos(block);
      var size = this.size;
      var half = size / 2;
      var gridChar = this.grid[block.x][block.y];

      ctx.lineWidth = 1;
      ctx.fillStyle = "black";
      ctx.strokeStyle = "black";

      //Render background;
      if (gridChar === 0) {
        ctx.fillRect(pos.x, pos.y, size, size);
      } else {
        ctx.strokeRect(pos.x, pos.y, size, size);
      }

      //Render character
      if (gridChar.length > 0) {
        ctx.textAlign = "center";
        ctx.font = Math.floor(size * .8) + "px Roboto";
        if (gridChar.indexOf('*') === 0) {
          ctx.fillStyle = "green";
          gridChar = gridChar.substr(1);
        } else ctx.fillStyle = "black";
        ctx.fillText(gridChar, pos.x + half, pos.y + half + size * .3);
      }

      //Render selection TODO: rendering of first and last bar
      if (this.selected && this.isInSelection(block)) {
        var dir = this.selected.d;
        var op = this.selected.d === "x" ? "y" : "x";
        ctx.lineWidth = this.size / 10;
        ctx.strokeStyle = "rgba(240,87,43,1)";
        var d = this.size / 20;

        ctx.beginPath();
        pos[op] += d;
        ctx.moveTo(pos.x, pos.y);
        pos[dir] += this.size;
        ctx.lineTo(pos.x, pos.y);
        pos[op] += this.size - 2 * d;
        if (block[dir] == this.selected[dir] + this.selected.word.length - 1) { //last block of word
          ctx.moveTo(pos.x, pos.y);
        } else ctx.moveTo(pos.x, pos.y);
        pos[dir] -= this.size;
        ctx.lineTo(pos.x, pos.y);
        pos[op] -= this.size - 2 * d;
        if (block[dir] == this.selected[dir]) { //first block of word
          ctx.moveTo(pos.x, pos.y);
        }
        ctx.stroke();
        pos[op] -= d;

        //Render focus
        if (this.selected[dir] + this.focus === block[dir]) {
          d = d * 2;
          ctx.beginPath();
          ctx.strokeStyle = "rgba(3,187,212,1)";
          ctx.moveTo(pos.x + 2 * d, pos.y + 8.5 * d);
          ctx.lineTo(pos.x + 8 * d, pos.y + 8.5 * d);
          ctx.stroke();
        }
      }
    }

    Crossword.prototype.acceptCharacter = function (block, character) {
      var size = this.size;
      this.grid[block.x][block.y] = '*' + character;
      this.userInput(block, '*');
      this.updateBlock(this.ctx, block);
    }

    Crossword.prototype.playSound = function (sound) {
      if (!this.audioContext) this.audioContext = this.openAudioContext();
      if (this.audio[sound]) {
        var buffer = this.audio[sound];
        var source = this.audioContext.createBufferSource();
        source.buffer = buffer;
        source.connect(this.audioGain);
        source.loop = false;
        source.start(0);
      } else this.loadAudio(sound, true);
    }

    Crossword.prototype.loadAudio = function (sound, playIfLoaded) {
      var request = new XMLHttpRequest();
      request.open('GET', sound, true);
      request.responseType = 'arraybuffer';

      var self = this;

      // Decode asynchronously
      // request.onload = function() {
      //   var context = self.audioContext;
      //   context.decodeAudioData(request.response, function(buffer) {
      //     self.audio[sound] = buffer;
      //     if(playIfLoaded) self.playSound(sound);
      //   });
      // }

      request.send();
    }

    Crossword.prototype.openAudioContext = function () {
      if (AudioContext) {
        if (!this.audio) this.audio = {};
        this.audioContext = new AudioContext();
        this.audioGain = this.audioContext.createGain();
        this.audioGain.value = 1;
        this.audioGain.connect(this.audioContext.destination);
      }
    }

    function blockComparator(a, b) {
      if (a.x != b.x) return a.x - b.x;
      else return a.y - b.y;
    }

    function isEqual(a, b) {
      if (a && b) {
        if (a.x == b.x && a.y == b.y) return true;
        else return false;
      } else return false;
    }

    return Crossword;
  }

  saveGame(){
    const message = JSON.stringify({
        activityForms:this.activityForms
    });
    window.parent.postMessage(message, '*');
  }

  resetGame(){
    if (this.leastPoints > 0){
      this.consoleLog("GameDump in ResetGame===>", this.gameDump);
      this.reset = true;
      this.addPropertiesAndActivity();
      this.consoleLog("Activities in ResetGame 2===>", JSON.stringify(this.activityForms));
      this.reset = false;
      this.saveGame();

    } else {
      alert("U heeft onvoldoende punten om een reset aan te vragen, punten: " + this.leastPoints);
    }
  }

  consoleLog(var1:any, var2:any){
    let isEnabled = true;
    if(isEnabled == true){
      //console.log(var1, var2);
    }
  }

}
