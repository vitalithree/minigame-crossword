import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrosswordPageRoutingModule } from './crossword-routing.module';

import { CrosswordPage } from './crossword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrosswordPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CrosswordPage]
})
export class CrosswordPageModule {}
