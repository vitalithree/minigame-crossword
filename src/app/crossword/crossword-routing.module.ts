import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrosswordPage } from './crossword.page';

const routes: Routes = [
  {
    path: '',
    component: CrosswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrosswordPageRoutingModule {}
